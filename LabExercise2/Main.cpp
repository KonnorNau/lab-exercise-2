
// Lab Exercise 2
// Konnor Nau

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;


enum Rank { Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace };
enum Suit { Clubs, Diamonds, Spades, Hearts };

struct Card
{
	Rank Rank;
	Suit Suit;

};


void PrintCard(Card card)
{
	switch (card.Rank)
	{
	case Two: cout << "The Two of "; break;
	case Three: cout << "The Three of "; break;
	case Four: cout << "The Four of "; break;
	case Five: cout << "The Five of "; break;
	case Six: cout << "The Six of "; break;
	case Seven: cout << "The Seven of "; break;
	case Eight: cout << "The Eight of "; break;
	case Nine: cout << "The Nine of "; break;
	case Ten: cout << "The Ten of "; break;
	case Jack: cout << "The Jack of "; break;
	case Queen: cout << "The Queen of "; break;
	case King: cout << "The King of "; break;
	case Ace: cout << "The Ace of "; break;
	}
	switch (card.Suit)
	{
	case Hearts: cout << "Hearts\n"; break;
	case Clubs: cout << "Clubs\n"; break;
	case Spades: cout << "Spades\n"; break;
	case Diamonds: cout << "Diamonds\n"; break;

	}
}

Card HighCard(Card card1, Card card2)
{
	if (card1.Rank > card2.Rank)
	{
		return card1;
	}
	else
	{
		return card2;
	}
}

int main()
{
	Card c1;
	c1.Rank = Ten;
	c1.Suit = Hearts;
	PrintCard(c1);

	Card c2;
	c2.Rank = Four;
	c2.Suit = Spades;

	PrintCard(HighCard(c1, c2));

	_getch();
	return 0;
}
